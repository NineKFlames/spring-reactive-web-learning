package cheberiak.artem.springreactive.controller;

import cheberiak.artem.springreactive.model.entity.Fruit;
import cheberiak.artem.springreactive.model.request.FruitRequest;
import cheberiak.artem.springreactive.model.response.FruitDto;
import cheberiak.artem.springreactive.service.FruitService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/fruit")
@RequiredArgsConstructor
public class FruitController {
    private final FruitService service;

    @GetMapping("all")
    public Flux<FruitDto> findAll(@RequestParam(required = false) String color) {
        if (color == null) {
            return service.findAll().map(FruitDto::ofFruit);
        } else {
            return service.findAllByColor(color).map(FruitDto::ofFruit);
        }
    }

    @GetMapping("{fruitId}")
    public Mono<Fruit> findById(@PathVariable Long fruitId) {
        return service.findById(fruitId);
    }

    @PostMapping(value = "new", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Fruit> createNew(@RequestBody @Valid FruitRequest request) {
        return service.create(request.asNewFruit());
    }

    @PatchMapping(value = "{fruitId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Fruit> update(@PathVariable Long fruitId, @RequestBody @Valid FruitRequest request) {
        return service.update(fruitId, request.getName(), request.getColor(), request.getAverageWeight());
    }

    @DeleteMapping("delete/{fruitId}")
    public Mono<Void> delete(@PathVariable Long fruitId) {
        return service.delete(fruitId);
    }
}
