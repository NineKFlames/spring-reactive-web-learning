package cheberiak.artem.springreactive.repository;

import cheberiak.artem.springreactive.model.entity.Fruit;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface ReactiveFruitRepository extends ReactiveCrudRepository<Fruit, Long> {
    // R2DBC repositories do not yet support query derivation.
    // https://docs.spring.io/spring-data/r2dbc/docs/1.0.0.M2/reference/html/#r2dbc.repositories.queries
    @Query("SELECT fruit.* FROM fruit WHERE color = $1")
    Flux<Fruit> findAllByColor(String color);
}
