package cheberiak.artem.springreactive.config;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@Configuration
@EnableR2dbcRepositories(basePackages = "")
@RequiredArgsConstructor
public class R2dbcConfig extends AbstractR2dbcConfiguration {
    private final PostgresR2dbcProperties postgresR2dbcProperties;

    @Bean
    @Override
    public PostgresqlConnectionFactory connectionFactory() {
        return new PostgresqlConnectionFactory(
                PostgresqlConnectionConfiguration.builder()
                                                 .host(postgresR2dbcProperties.getHost())
                                                 .database(postgresR2dbcProperties.getDatabase())
                                                 .username(postgresR2dbcProperties.getUsername())
                                                 .password(postgresR2dbcProperties.getPassword())
                                                 .port(postgresR2dbcProperties.getPort())
                                                 .build());
    }
}
