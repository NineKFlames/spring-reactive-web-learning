package cheberiak.artem.springreactive.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@ConfigurationProperties(prefix = "datasource")
@Component
@Getter
@Setter
@Validated
public class PostgresR2dbcProperties {
    @NotEmpty
    private String host;

    @Positive
    @NotNull
    private Integer port;

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;

    @NotEmpty
    private String database;
}
