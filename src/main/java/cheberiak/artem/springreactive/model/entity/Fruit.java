package cheberiak.artem.springreactive.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;

@Table("fruit")
@Data
@AllArgsConstructor
public class Fruit {
    @Id
    private Long id;
    @Column("name")
    private String name;
    @Column("color")
    private String color;
    @Column("averageweight")
    private BigDecimal averageWeight;

    public Fruit update(String name, String color, BigDecimal averageWeight) {
        return new Fruit(this.id,
                         name == null ? getName() : name,
                         color == null ? getColor() : color,
                         averageWeight == null ? getAverageWeight() : averageWeight
        );
    }
}
