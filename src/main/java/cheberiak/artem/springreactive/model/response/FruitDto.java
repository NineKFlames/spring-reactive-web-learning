package cheberiak.artem.springreactive.model.response;

import cheberiak.artem.springreactive.model.entity.Fruit;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.math.BigDecimal;

@JsonSerialize
@Data
public class FruitDto {
    private final Long id;
    private final String name;
    private final String color;
    private final BigDecimal averageWeight;

    public static FruitDto ofFruit(Fruit fruit) {
        return new FruitDto(fruit.getId(), fruit.getName(), fruit.getColor(), fruit.getAverageWeight());
    }
}
