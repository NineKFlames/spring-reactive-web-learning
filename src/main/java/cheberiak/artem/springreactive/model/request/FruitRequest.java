package cheberiak.artem.springreactive.model.request;

import cheberiak.artem.springreactive.model.entity.Fruit;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@JsonDeserialize
@Data
public class FruitRequest {
    private final String name;
    private final String color;
    @Positive
    private final BigDecimal averageWeight;

    public Fruit asNewFruit() {
        return new Fruit(null, name, color, averageWeight);
    }
}
