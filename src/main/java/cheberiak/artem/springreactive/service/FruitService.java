package cheberiak.artem.springreactive.service;

import cheberiak.artem.springreactive.model.entity.Fruit;
import cheberiak.artem.springreactive.repository.ReactiveFruitRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
@Slf4j
public class FruitService {
    private final ReactiveFruitRepository repository;

    public Flux<Fruit> findAll() {
        return repository.findAll();
    }

    public Mono<Fruit> findById(Long id) {
        return repository.findById(id);
    }

    public Flux<Fruit> findAllByColor(String color) {
        return repository.findAllByColor(color);
    }

    public Mono<Fruit> create(Fruit fruit) {
        return repository.save(fruit);
    }

    public Mono<Void> delete(Long fruitId) {
        return repository.deleteById(fruitId);
    }

    public Mono<Fruit> update(Long fruitId, String name, String color, BigDecimal averageWeight) {
        return repository.findById(fruitId)
                         .map(fruit -> fruit.update(name, color, averageWeight))
                         .flatMap(repository::save)
                         .doOnNext(fruit -> log.info(fruit.toString()));
    }
}
