# Spring Boot WebFlux + R2DBC example project  
This is a small reactive CRUD application for storing fruit data.

### Prerequisites:  
* JDK 11
* Maven
* Docker + Docker-compose

### How to run this project (Unix-like):
0. Spin up the DB:  
`docker-compose up -d`
0. Run the migration and start the server:  
`mvn initialize liquibase:update spring-boot:run`